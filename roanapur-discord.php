<?php
/**
 * SPDX-FileCopyrightText: 2019 Jane Doe <jane@example.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Roanapur Discord
 *
 * Copyright (C) 2021 Benjamin Kahlau
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author    Benjamin Kahlau <benjamin.kahlau@roanapur.de>
 * @copyright 2021 Benjamin Kahlau
 * @license   GPL-3.0
 * @link      https://github.com/Terenc3/roanpur-discord
 * @package   RoanpurDiscord
 * @version   1.0.0
 *
 * @wordpress-plugin
 * Plugin Name: Roanapur Discord
 * Plugin URI:     https://github.com/Terenc3/roanpur-discord
 * Description: Add discord profiles to wordpress
 * Version:     1.0.0
 * Author:         Benjamin Kahlau
 * Author URI:     http://roanapur.de
 * Text Domain: roanapur-discord
 * Domain Path: /languages/
 * License:     GPL-3.0
 * License URI: https://www.gnu.org/licenses/gpl-3.0.txt
 */
if (! defined('ABSPATH') ) {
    exit;
}

require_once 'src/roanapur-discord.php';
require_once 'src/roanapur-discord-widget.php';

add_action('init', array('RoanapurDiscord', 'init'));
add_action('widgets_init', array('RoanapurDiscordWidget', 'load_widget'));
?>
