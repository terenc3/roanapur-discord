/**
 * SPDX-FileCopyrightText: 2019 Jane Doe <jane@example.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This file is part of Roanapur Discord.
 *
 * Roanapur Discord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Roanapur Discord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License:
 * along with Roanapur Discord. If not, see <http://www.gnu.org/licenses/>.\n
 */
/* global jQuery, roanapurDiscord */
;(function ($) {
	'use strict'

	$(document).ready(function () {
		$('.roanapur-discord').each(function () {
			const el = $(this),
				id = el.data('id'),
				url = el.data('url')

			$.get(
				roanapurDiscord.url,
				{
					action: roanapurDiscord.action,
					serverId: id
				},
				function (data) {
					if (data.message) {
						const error = document.createElement('span')
						error.innerText = 'Error: ' + data.message
						el.append(error)
						return
					}

					if (data.guild_id) {
						const error = document.createElement('span')
						error.innerText = 'Error: ' + data.guild_id[0]
						el.append(error)
						return
					}

					const fragment = document.createDocumentFragment()

					const icon = document.createElement('i')
					icon.style.color = '#738adb'
					icon.style.marginRight = '0.25em'
					icon.className = 'fab fa-discord'
					fragment.appendChild(icon)

					let name
					if (url) {
						name = document.createElement('a')
						name.setAttribute('href', url)
					} else {
						name = document.createElement('span')
					}
					name.style.fontWeight = 'bold'
					name.style.marginRight = '0.25em'
					name.innerText = data.name
					fragment.appendChild(name)

					const presence = document.createElement('span')
					presence.style.float = 'right'
					presence.style.marginRight = '0.25em'

					const count = document.createElement('span')
					count.innerText = data.presence_count
					count.style.marginRight = '0.25em'
					presence.appendChild(count)

					const indicator = document.createElement('i')
					indicator.innerText = '⬤'
					indicator.style.color = '#eb0400'
					presence.appendChild(indicator)

					fragment.appendChild(presence)

					el.append(fragment)
				}
			)
		})
	})
})(jQuery)
