<?php
/**
 * SPDX-FileCopyrightText: 2019 Jane Doe <jane@example.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This file is part of Roanapur Discord.
 *
 * Roanapur Discord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Roanapur Discord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Roanapur Discord. If not, see <http://www.gnu.org/licenses/>.\n
 */
class RoanapurDiscordWidget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct(
            'roanapur-discord-widget',
            'Discord',
            array(
            'description' => 'Zeigt ein Discord Server Widget'
            )
        );
    }



    public static function load_widget()
    {
        register_widget(__CLASS__);
    }

    public function widget($args, $instance)
    {
        $title = !empty($instance['title']) ? $instance['title'] : 'Discord';

        $title = apply_filters('widget_title', $title);

        echo $args['before_widget'];
        if (!empty($title)) {
            echo $args['before_title'].$title.$args['after_title'];
        }
        echo sprintf('<div class="roanapur-discord" data-id="%s" data-url="%s"></div>', $instance['id'], $instance['url']);
        echo $args['after_widget'];

        wp_enqueue_script('roanapur-discord');
    }

    public function form($instance)
    {
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __('Title:') ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title') ?>" name="<?php echo $this->get_field_name('title') ?>" type="text" value="<?php echo esc_attr(isset($instance['title']) ? $instance['title'] : "") ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('id'); ?>"><?php echo __('Server ID:') ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('id') ?>" name="<?php echo $this->get_field_name('id') ?>" type="text" value="<?php echo esc_attr(isset($instance['id']) ? $instance['id'] : "") ?>" required pattern="[0-9]+" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('url'); ?>"><?php echo __('Invitate Link:') ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('url') ?>" name="<?php echo $this->get_field_name('url') ?>" type="url" value="<?php echo esc_attr(
                isset($instance['url']) ? $instance['url'] : ""
            ) ?>" />
        </p>
        <?php
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['id'] = (!empty($new_instance['id'])) ? strip_tags($new_instance['id']) : '';
        $instance['url'] = (!empty($new_instance['url'])) ? strip_tags($new_instance['url']) : '';
        return $instance;
    }
}
?>
