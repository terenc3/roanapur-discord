<?php
/**
 * SPDX-FileCopyrightText: 2019 Jane Doe <jane@example.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This file is part of Roanapur Discord.
 *
 * Roanapur Discord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Roanapur Discord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Roanapur Discord. If not, see <http://www.gnu.org/licenses/>.\n
 */
class RoanapurDiscord
{
    private static $version = '1.0';
    private static $plugin ='roanapur-discord';
    private static $slug = 'discord';

    private static $json_action = 'get_discord_widget';

    public static function init()
    {
        load_plugin_textdomain(self::$plugin, false, basename(dirname(__FILE__)) . '/languages');

        add_shortcode('roanapur-discord-widget', array(__CLASS__, 'shortcode'));

        add_action('wp_enqueue_scripts', array(__CLASS__, 'hook_ajax_script'));
        add_action('admin_enqueue_scripts', array(__CLASS__, 'hook_ajax_script'));
        add_action('wp_ajax_'.self::$json_action, array(__CLASS__, 'get_discord_widget'));
        add_action('wp_ajax_nopriv_'.self::$json_action, array(__CLASS__, 'get_discord_widget'));
    }

    // Shortcode
    public static function shortcode( $atts )
    {
        if (!$atts['id']) {
            return __("Server not found!", "roanapur-discord");
        }

        return sprintf('<div class="roanapur-discord" data-id="%s" data-url="%s"></div>', $atts['id'], $atts['url']);
    }

    public static function hook_ajax_script()
    {
        wp_register_script('roanapur-discord', plugin_dir_url(__FILE__)  . 'roanapur-discord.js', array( 'jquery' ), '1.0.0', true);
        wp_enqueue_script('roanapur-discord');
        wp_localize_script(
            'roanapur-discord', 'roanapurDiscord', array(
            'url' => admin_url('admin-ajax.php'),
            'action' => self::$json_action
            )
        );
    }

    /**
     * Retrieve profile from discord web api
     */
    public static function get_discord_widget()
    {
        header('Content-type: application/json');
        $ch = curl_init();
        curl_setopt_array(
            $ch, array(
            CURLOPT_URL => 'https://discord.com/api/guilds/'.$_GET['serverId'].'/widget.json'
            )
        );
        curl_exec($ch);
        curl_close($ch);
        die();
    }
}