<!--
SPDX-FileCopyrightText: 2019 Jane Doe <jane@example.com>
SPDX-License-Identifier: GPL-3.0-or-later
-->
# Roanapur Discord
> Wordpress Plugin to show Discord Server Widget as Widget (legacy) or Shortcode
> 
## Usage
+ Install plugin
+ Activate Widget in Discord Server Settings [Discord Blog](https://blog.discord.com/add-the-discord-widget-to-your-site-d45ffcd718c6)
+ Get the server id [Discord Support](https://support.discord.com/hc/de/articles/206346498-Wie-finde-ich-meine-Server-ID-)
